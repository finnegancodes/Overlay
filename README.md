# Overlay

![Overlay](https://gitlab.com/finnegancodes/Overlay/-/raw/6b8bea157caf952314407f95bb7af43205c2202f/Screenshots/overlay.png)

An iOS app that is as simple as it sounds! Just take any image, or make a blank one, and *overlay* it with text.

"What is this useful for?" I hear you ask. Well, I specifically made it for those times I quickly need to change some text on a meme. Firing up a full-fledged image editor like GIMP feels a bit over kill for such a simple task, so I made this little app.

## How does it work?

Under the hood, there is no real image editing involved. Overlay simply uses SwiftUI views which it overlays on top of each other; it doesn't edit pixel data of the image directly. Then, when you go to export your creation, the app takes a "screenshot" of the view hierarchy you created by dragging layers around, and spits out *that* as the final image.

It's actually a little more complicated than that, because when you are "editing" the image, Overlay needs to scale it down (or up) to fit the screen, and when you export it, the image is scaled back up to its natural size, which requires that everything else is scaled up with it, and that becomes a particularly annoying with fonts...but luckily, that is my problem. For you it just works. :)

## Compatibility

| Platform | Minimum version  |
| ----- | ------ |
| iOS/iPadOS | 17.0 |

