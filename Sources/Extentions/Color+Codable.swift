//
//  Color+Codable.swift
//  Overlay
//
//  Created by Finnegan on 19/09/2024.
//

import SwiftUI

extension Color {
    static func encodeColor(_ color: Color) throws -> Data {
        let uiColor = UIColor(color)
        return try NSKeyedArchiver.archivedData(
            withRootObject: uiColor,
            requiringSecureCoding: true
        )
    }
    
    static func decodeColor(from data: Data) throws -> Color {
        guard let uiColor = try NSKeyedUnarchiver
            .unarchivedObject(ofClass: UIColor.self, from: data)
        else {
            throw DecodingError.wrongType
        }
        return Color(uiColor: uiColor)
    }
    
    enum DecodingError: Error {
        case wrongType
    }
}

extension Color: Codable {
    public init(from decoder: any Decoder) throws {
        let container = try decoder.singleValueContainer()
        let data = try container.decode(Data.self)
        let color = try Self.decodeColor(from: data)
        self.init(color)
    }
    
    public func encode(to encoder: any Encoder) throws {
        var container = encoder.singleValueContainer()
        let data = try Self.encodeColor(self)
        try container.encode(data)
    }
}
