//
//  View+SafeAreaDependantPadding.swift
//  Overlay
//
//  Created by Finnegan on 19/09/2024.
//

import SwiftUI

struct SafeAreaDependentPadding: ViewModifier {
    
    var hasBottomSafeAreaPadding: Bool {
        let window = UIApplication
            .shared
            .connectedScenes
            .compactMap { $0 as? UIWindowScene }
            .flatMap { $0.windows }
            .first { $0.isKeyWindow }
        guard let safeAreaBottom =  window?.safeAreaInsets.bottom else {
            return false
        }
        return safeAreaBottom > 0
    }
    
    func body(content: Content) -> some View {
        if hasBottomSafeAreaPadding {
            content.padding(.horizontal)
        } else {
            content.padding()
        }
    }
}

extension View {
    func safeAreaDependentPadding() -> some View {
        modifier(SafeAreaDependentPadding())
    }
}
