//
//  Bundle+Version.swift
//  Overlay
//
//  Created by Adam Oravec on 28/03/2024.
//

import Foundation

extension Bundle {
    var releaseVersionNumber: String {
        return (infoDictionary?["CFBundleShortVersionString"] as? String) ?? "0"
    }
    var buildVersionNumber: String {
        return (infoDictionary?["CFBundleVersion"] as? String) ?? "0"
    }
}
