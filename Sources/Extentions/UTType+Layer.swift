//
//  UTType+Layer.swift
//  Overlay
//
//  Created by Finnegan on 19/09/2024.
//

import UniformTypeIdentifiers

extension UTType {
    static let layer = UTType("eu.finnswonderland.layer")!
}
