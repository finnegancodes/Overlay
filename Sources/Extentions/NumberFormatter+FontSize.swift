//
//  NumberFormatter+FontSize.swift
//  Overlay
//
//  Created by Adam Oravec on 15/02/2024.
//

import Foundation

extension NumberFormatter {
    static let fontSize: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .none
        formatter.allowsFloats = false
        formatter.maximum = 128
        formatter.minimum = 11
        return formatter
    }()
}
