//
//  OverlayApp.swift
//  Overlay
//
//  Created by Adam Oravec on 13/02/2024.
//

import SwiftUI

@main
struct OverlayApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
