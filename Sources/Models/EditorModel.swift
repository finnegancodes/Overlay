//
//  EditorModel.swift
//  Overlay
//
//  Created by Adam Oravec on 13/02/2024.
//

import SwiftUI
import UniformTypeIdentifiers

@Observable
class EditorModel {
    
    var scale: Double = 1
    var layers: [Layer] = []
    var layerSelection: Layer?
    var inspectorSelection: Inspector = .layerList
    var showingResultSheet = false
    var hasLayerToPaste = false
    
    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(pasteboardChanged), name: UIPasteboard.changedNotification, object: nil)
        pasteboardChanged(sender: NSNotification(name: UIPasteboard.changedNotification, object: nil))
    }
    
    func deleteLayer(at offset: Int) {
        if layers[offset] == layerSelection {
            layerSelection = nil
        }
        layers.remove(at: offset)
    }
    
    func deleteLayer(_ layer: Layer) {
        guard let offset = layers.firstIndex(of: layer) else { return }
        deleteLayer(at: offset)
    }
    
    func duplicateLayer(_ layer: Layer) {
        let newLayer = layer.duplicate()
        layers.append(newLayer)
        layerSelection = newLayer
    }
    
    func addCopiedLayer() {
        guard hasLayerToPaste else { return }
        let pasteboard = UIPasteboard.general
        if let pasteboardData = pasteboard.data(forPasteboardType: UTType.layer.identifier) {
            guard let layer = try? JSONDecoder().decode(Layer.self, from: pasteboardData) else { return }
            print("Found layer in pasteboard:", layer.id)
            layers.append(layer)
            layerSelection = layer
        }
    }
    
    @objc func pasteboardChanged(sender: NSNotification) {
        let pasteboard = UIPasteboard.general
        hasLayerToPaste = pasteboard.contains(pasteboardTypes: [UTType.layer.identifier])
    }
    
    deinit {
        layerSelection = nil
        layers = []
    }
}

extension EditorModel {
    enum Inspector: String, CaseIterable {
        case layerList = "layers"
        case frameEditor = "frame"
        case fontEditor = "font"
        case textEditor = "text"
    }
}
