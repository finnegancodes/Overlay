//
//  WrappedImage.swift
//  Overlay
//
//  Created by Adam Oravec on 14/02/2024.
//

import SwiftUI

struct WrappedImage: Identifiable, Transferable {
    let id: UUID = UUID()
    let data: Data
    
    init(_ data: Data) {
        self.data = data
    }
    
    static var transferRepresentation: some TransferRepresentation {
        DataRepresentation(exportedContentType: .jpeg) { image in
            image.data
        }
    }
}
