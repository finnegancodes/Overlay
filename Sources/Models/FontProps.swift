//
//  FontProps.swift
//  Overlay
//
//  Created by Adam Oravec on 14/02/2024.
//

import SwiftUI

struct FontProps: Codable {
    var name: Name = .stock
    var size: CGFloat = 18
    var isBold: Bool = false
    var design: Design = .regular
    var alignment: Alignment = .center
    var isItalic: Bool = false
    var color: Color = .black
}

extension FontProps {
    enum Name: String, CaseIterable, Codable {
        case stock = "default"
        case impact
    }
}

extension FontProps {
    enum Weight: Int, CaseIterable {
        case thin
        case regular
        case semibold
        case bold
        case black
        
        var raw: Font.Weight {
            switch self {
            case .thin:
                return .thin
            case .regular:
                return .regular
            case .semibold:
                return .semibold
            case .bold:
                return .bold
            case .black:
                return .black
            }
        }
        
        var label: String {
            switch self {
            case .thin:
                return "thin"
            case .regular:
                return "regular"
            case .semibold:
                return "semibold"
            case .bold:
                return "bold"
            case .black:
                return "black"
            }
        }
    }
}

extension FontProps {
    enum Design: Int, Codable, CaseIterable {
        case regular
        case monospaced
        case rounded
        case serif
        
        var raw: Font.Design {
            switch self {
            case .regular:
                return .default
            case .monospaced:
                return .monospaced
            case .rounded:
                return .rounded
            case .serif:
                return .serif
            }
        }
        
        var label: String {
            switch self {
            case .regular:
                return "regular"
            case .monospaced:
                return "monospaced"
            case .rounded:
                return "rounded"
            case .serif:
                return "serif"
            }
        }
    }
}

extension FontProps {
    enum Alignment: Int, Codable, CaseIterable {
        case trailing
        case center
        case leading
        
        var raw: TextAlignment {
            switch self {
            case .leading:
                return .leading
            case .center:
                return .center
            case .trailing:
                return .trailing
            }
        }
        
        var label: String {
            switch self {
            case .leading:
                return "leading"
            case .center:
                return "center"
            case .trailing:
                return "trailing"
            }
        }
        
        var iconName: String {
            switch self {
            case .leading:
                return "text.alignleft"
            case .center:
                return "text.aligncenter"
            case .trailing:
                return "text.alignright"
            }
        }
    }
}
