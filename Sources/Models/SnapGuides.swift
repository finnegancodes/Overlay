//
//  SnapGuides.swift
//  Overlay
//
//  Created by Adam Oravec on 14/02/2024.
//

struct SnapGuides {
    var x = false
    var y = false
    
    mutating func off() {
        x = false
        y = false
    }
}

extension SnapGuides: Equatable {
    
}
