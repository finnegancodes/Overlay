//
//  Layer.swift
//  Overlay
//
//  Created by Adam Oravec on 13/02/2024.
//

import SwiftUI

@Observable
class Layer: Codable {
    let id: UUID = UUID()
    var title: String = "New Layer"
    var text: String = "Text"
    var position: CGPoint = CGPoint(x: 0.5, y: 0.5)
    var size: CGSize = CGSize(width: 0.5, height: 0.2)
    var font = FontProps()
    var background: Color = .white
    var shadow: CGFloat = 0
}

extension Layer: Identifiable {}

extension Layer: Equatable {
    static func == (lhs: Layer, rhs: Layer) -> Bool {
        lhs.id == rhs.id
    }
}

extension Layer: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

extension Layer: Transferable {
    static var transferRepresentation: some TransferRepresentation {
        CodableRepresentation(contentType: .layer)
    }
}

extension Layer {
    func duplicate() -> Layer {
        let newLayer = Layer()
        newLayer.title = self.title
        newLayer.text = self.text
        newLayer.position = self.position
        newLayer.size = self.size
        newLayer.font = self.font
        newLayer.background = self.background
        newLayer.shadow = self.shadow
        return newLayer
    }
    
    func encoded() throws -> Data {
        try JSONEncoder().encode(self)
    }
}
