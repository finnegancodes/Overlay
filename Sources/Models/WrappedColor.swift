//
//  WrappedColor.swift
//  Overlay
//
//  Created by Adam Oravec on 16/02/2024.
//

import SwiftUI

enum WrappedColor: Int, CaseIterable, Codable {
    case red, orange, yellow, green, blue, purple, pink, white, black
    
    var raw: Color {
        switch self {
        case .red:
            return .red
        case .orange:
            return .orange
        case .yellow:
            return .yellow
        case .green:
            return .green
        case .blue:
            return .blue
        case .purple:
            return .purple
        case .pink:
            return .pink
        case .white:
            return .white
        case .black:
            return .black
        }
    }
}

extension Color {
    static let baseColors: [Color] = [.red, .orange, .yellow, .green, .cyan, .blue, .indigo, .purple, .pink, .white, .black]
}
