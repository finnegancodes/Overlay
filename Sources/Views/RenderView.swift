//
//  RenderView.swift
//  Overlay
//
//  Created by Adam Oravec on 14/02/2024.
//

import SwiftUI

struct RenderView: View {
    
    let image: UIImage
    let editorModel: EditorModel
    
    var body: some View {
        Image(uiImage: image)
            .overlay {
                ForEach(editorModel.layers) { layer in
                    EditorCanvasLayerView(layer, size: image.size, scale: calculateScale(from: image.size), isSelected: false, snapGuides: .constant(SnapGuides())) {
                        
                    } onEdit: {
                        
                    }
                }
            }
            .coordinateSpace(name: "canvas")
            .clipped()
    }
    
    private func calculateScale(from size: CGSize) -> Double {
        let canvasWidth = size.width * editorModel.scale
        let scale = image.size.width / canvasWidth
        return scale
    }
}

#Preview {
    RenderView(image: UIImage(), editorModel: EditorModel())
}
