//
//  EditorCanvasInspector.swift
//  Overlay
//
//  Created by Finnegan on 18/09/2024.
//

import SwiftUI

struct EditorCanvasInspector: View {
    
    @Bindable var editorModel: EditorModel
    
    var body: some View {
        VStack {
            Picker("Inspector", selection: $editorModel.inspectorSelection) {
                ForEach(EditorModel.Inspector.allCases, id: \.rawValue) { inspector in
                    Text(inspector.rawValue.capitalized).tag(inspector)
                }
            }
            .pickerStyle(.segmented)
            .padding()
            switch editorModel.inspectorSelection {
            case .layerList:
                EditorLayerList(editorModel: editorModel)
            case .frameEditor:
                LayerFrameEditor(editorModel: editorModel)
                    .padding(.horizontal)
            case .fontEditor:
                LayerFontEditor(editorModel: editorModel)
                    .padding(.horizontal)
            case .textEditor:
                LayerTextEditor(editorModel: editorModel)
                    .padding(.horizontal)
            }
        }
    }
}

#Preview {
    EditorCanvasInspector(editorModel: EditorModel())
}
