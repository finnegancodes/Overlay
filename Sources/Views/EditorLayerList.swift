//
//  EditorLayerList.swift
//  Overlay
//
//  Created by Adam Oravec on 14/02/2024.
//

import SwiftUI
import UniformTypeIdentifiers

struct EditorLayerList: View {
    
    @Bindable var editorModel: EditorModel
    
    @State private var showingNewLayerAlert = false
    @State private var newLayerTitle = ""
    
    @State private var showingRenameLayerAlert = false
    @State private var layerToRename: Layer?
    @State private var renameTitle = ""
    
    @Environment(\.colorScheme) private var colorScheme
    
    var body: some View {
        List(selection: $editorModel.layerSelection) {
            ForEach($editorModel.layers) { $layer in
                Text(layer.title)
                    .tag(layer)
                    .contextMenu {
                        Section {
                            copyButton(for: layer)
                            duplicateButton(for: layer)
                            renameButton(for: layer)
                        }
                        Section {
                            deleteButton(for: layer)
                        }
                    }
                    .listRowBackground(backgroundForCell(layer))
            }
            .onDelete(perform: deleteLayers)
            .onMove(perform: moveLayers)
        }
        .listStyle(.plain)
        .overlay(alignment: .bottomTrailing) {
            HStack(spacing: 10) {
                pasteButton
                    .disabled(editorModel.hasLayerToPaste == false)
                addButton
            }
            .padding()
        }
        .overlay {
            if editorModel.layers.isEmpty {
                Text("No Layers")
                    .font(.subheadline)
                    .foregroundStyle(.secondary)
            }
        }
        .animation(.snappy, value: editorModel.layers)
        .alert("New Layer", isPresented: $showingNewLayerAlert) {
            TextField("Title", text: $newLayerTitle)
            Button("Cancel", role: .cancel, action: cancelNewLayer)
            Button("Add", action: addLayer)
        } message: {
            Text("Enter a name for the new layer.")
        }
        .alert("Rename Layer", isPresented: $showingRenameLayerAlert) {
            TextField("Title", text: $renameTitle)
            Button("Cancel", role: .cancel, action: cancelRename)
            Button("Rename", action: renameLayer)
        } message: {
            Text("Enter a new name for the layer.")
        }
    }
    
    var addButton: some View {
        Button {
            showingNewLayerAlert = true
        } label: {
            Image(systemName: "plus")
                .accessibilityLabel("New Layer")
                .fontWeight(.medium)
                .padding(8)
                .background(.thinMaterial)
                .clipShape(Circle())
        }
        .buttonStyle(.borderless)
    }
    
    var pasteButton: some View {
        Button {
            editorModel.addCopiedLayer()
        } label: {
            Image(systemName: "doc.on.clipboard")
                .accessibilityLabel("Paste Layer")
                .padding(8)
                .background(.thinMaterial)
                .clipShape(Circle())
        }
        .buttonStyle(.borderless)
    }
    
    @ViewBuilder
    func copyButton(for layer: Layer) -> some View {
        Button {
            copyLayer(layer)
        } label: {
            Label("Copy", systemImage: "doc.on.doc")
        }
    }
    
    @ViewBuilder
    func duplicateButton(for layer: Layer) -> some View {
        Button {
            editorModel.duplicateLayer(layer)
        } label: {
            Label("Duplicate", systemImage: "doc.on.doc")
        }
    }
    
    @ViewBuilder
    func renameButton(for layer: Layer) -> some View {
        Button {
            layerToRename = layer
            renameTitle = layer.title
            showingRenameLayerAlert = true
        } label: {
            Label("Rename", systemImage: "pencil")
        }
    }
    
    @ViewBuilder
    func deleteButton(for layer: Layer) -> some View {
        Button(role: .destructive) {
            editorModel.deleteLayer(layer)
        } label: {
            Label("Delete", systemImage: "trash")
        }
    }
    
    @ViewBuilder
    func backgroundForCell(_ layer: Layer) -> some View {
        if editorModel.layerSelection == layer {
            if colorScheme == .light {
                Rectangle().fill(.background.secondary)
            } else {
                Rectangle().fill(.background.tertiary)
            }
        } else {
            if colorScheme == .light {
                Rectangle().fill(.background)
            } else {
                Rectangle().fill(.background.secondary)
            }
        }
    }
    
    private func deleteLayers(at offsets: IndexSet) {
        for i in offsets {
            editorModel.deleteLayer(at: i)
        }
    }
    
    private func moveLayers(from offsets: IndexSet, to index: Int) {
        editorModel.layers.move(fromOffsets: offsets, toOffset: index)
    }
    
    private func addLayer() {
        guard !newLayerTitle.isEmpty else { return }
        let layer = Layer()
        layer.title = newLayerTitle
        editorModel.layers.append(layer)
        editorModel.layerSelection = layer
        newLayerTitle = ""
    }
    
    private func renameLayer() {
        guard !renameTitle.isEmpty else { return }
        guard let layerToRename else { return }
        
        layerToRename.title = renameTitle
        self.renameTitle = ""
        self.layerToRename = nil
    }
    
    private func cancelNewLayer() {
        newLayerTitle = ""
    }
    
    private func cancelRename() {
        renameTitle = ""
        layerToRename = nil
    }
    
    private func copyLayer(_ layer: Layer) {
        let pasteboard = UIPasteboard.general
        guard let encodedLayer = try? layer.encoded() else {
            print("Failed to encode layer:", layer.id)
            return
        }
        pasteboard.setData(encodedLayer, forPasteboardType: UTType.layer.identifier)
        
    }
}

#Preview {
    EditorLayerList(editorModel: EditorModel())
}
