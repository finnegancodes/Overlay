//
//  SizeForm.swift
//  Overlay
//
//  Created by Adam Oravec on 27/04/2024.
//

import SwiftUI

struct NewImageForm: View {
    
    let onCreate: (UIImage) -> Void
    
    @State private var width = 500
    @State private var height = 500
    @State private var color: Color = .white
    
    @FocusState private var focusedField: FocusedField?
    
    @Environment(\.dismiss) private var dismiss
    
    var isSizeValid: Bool {
        width > 0 && height > 0 && width < 10_000 && height < 10_000
    }
    
    var body: some View {
        NavigationStack {
            Form {
                LabeledContent("Width") {
                    TextField("0", value: $width, format: .number)
                        .multilineTextAlignment(.trailing)
                        .focused($focusedField, equals: .width)
                    Text("px")
                }
                LabeledContent("Height") {
                    TextField("0", value: $height, format: .number)
                        .multilineTextAlignment(.trailing)
                        .focused($focusedField, equals: .height)
                    Text("px")
                }
                
                Section {
                    ColorPicker("Background Color", selection: $color)
                }
            }
            .navigationTitle("New Image")
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .cancellationAction) {
                    Button("Cancel", role: .cancel) {
                        dismiss()
                    }
                }
                ToolbarItem(placement: .confirmationAction) {
                    Button("Create") {
                        dismiss()
                        createBlankImage(CGSize(width: CGFloat(width), height: CGFloat(height)))
                    }
                    .disabled(!isSizeValid)
                }
            }
            .onAppear {
                focusedField = .width
            }
        }
    }
    
    private func createBlankImage(_ size: CGSize) {
        let renderer = UIGraphicsImageRenderer(size: size)
        let image = renderer.image { ctx in
            UIColor(color).setFill()
            ctx.fill(CGRect(origin: CGPoint(x: 0, y: 0), size: size))
        }
        onCreate(image)
    }
}

extension NewImageForm {
    enum FocusedField: Hashable {
        case width
        case height
    }
}

#Preview {
    NewImageForm { _ in }
}
