//
//  LayerFontEditor.swift
//  Overlay
//
//  Created by Adam Oravec on 10/03/2024.
//

import SwiftUI

struct LayerFontEditor: View {
    
    @Bindable var editorModel: EditorModel
    
    @Environment(\.colorScheme) private var colorScheme
    
    @State private var showingColorPicker = false
    @State private var shadowValues: [CGFloat] = [0, 1, 2, 3, 4]
    @State private var fontSizes: [CGFloat] = [12, 14, 18, 32, 64, 72, 128, 160, 200]
    
    var body: some View {
        ScrollView {
            if let layer = editorModel.layerSelection {
                @Bindable var layer = layer
                Grid {
                    GridRow {
                        Button {
                            layer.font.isBold.toggle()
                        } label: {
                            Image(systemName: "bold")
                                .frame(maxWidth: .infinity)
                                .frame(height: 35)
                                .background(layer.font.isBold ? Color.accentColor : .gray.opacity(0.15))
                                .foregroundStyle(layer.font.isBold ? .white : .accentColor)
                                .clipShape(RoundedRectangle(cornerRadius: 6))
                        }
                        .buttonStyle(.borderless)
                        Button {
                            layer.font.isItalic.toggle()
                        } label: {
                            Image(systemName: "italic")
                                .frame(maxWidth: .infinity)
                                .frame(height: 35)
                                .background(layer.font.isItalic ? Color.accentColor : .gray.opacity(0.15))
                                .foregroundStyle(layer.font.isItalic ? .white : .accentColor)
                                .clipShape(RoundedRectangle(cornerRadius: 6))
                        }
                        .buttonStyle(.borderless)
                        Menu {
                            Picker(selection: $layer.font.size) {
                                ForEach(fontSizes, id: \.self) { size in
                                    Text(String(format: "%.0f px", size))
                                        .tag(size)
                                }
                            } label: {}
                        } label: {
                            HStack(spacing: 2) {
                                Image(systemName: "arrow.up.and.down")
                                Text(String(format: "%.0f px", layer.font.size))
                            }
                            .frame(maxWidth: .infinity)
                            .frame(height: 35)
                            .background(.gray.opacity(0.15))
                            .foregroundStyle(.primary)
                            .clipShape(RoundedRectangle(cornerRadius: 6))
                        }
                        Menu {
                            Picker(selection: $layer.font.design) {
                                ForEach(FontProps.Design.allCases, id: \.rawValue) { design in
                                    Text(design.label.capitalized)
                                        .tag(design)
                                }
                            } label: {}
                        } label: {
                            HStack(spacing: 2) {
                                Image(systemName: "arrow.up.and.down")
                                Image(systemName: "scribble.variable")
                            }
                            .frame(maxWidth: .infinity)
                            .frame(height: 35)
                            .background(.gray.opacity(0.15))
                            .foregroundStyle(.primary)
                            .clipShape(RoundedRectangle(cornerRadius: 6))
                        }
                    }
                    GridRow {
                        ColorPickerButton(selection: $layer.font.color)
                        Menu {
                            Picker(selection: $layer.font.alignment) {
                                ForEach(FontProps.Alignment.allCases, id: \.rawValue) { alignment in
                                    Text(alignment.label.capitalized)
                                        .tag(alignment)
                                }
                            } label: {}
                        } label: {
                            HStack(spacing: 2) {
                                Image(systemName: "arrow.up.and.down")
                                Image(systemName: layer.font.alignment.iconName)
                            }
                            .frame(maxWidth: .infinity)
                            .frame(height: 35)
                            .background(.gray.opacity(0.15))
                            .foregroundStyle(.primary)
                            .clipShape(RoundedRectangle(cornerRadius: 6))
                        }
                        Menu {
                            Picker(selection: $layer.font.name) {
                                ForEach(FontProps.Name.allCases, id: \.rawValue) { font in
                                    Text(font.rawValue.capitalized)
                                        .tag(font)
                                }
                            } label: {}
                        } label: {
                            HStack(spacing: 2) {
                                Image(systemName: "arrow.up.and.down")
                                Text(layer.font.name.rawValue.capitalized)
                            }
                            .frame(maxWidth: .infinity)
                            .frame(height: 35)
                            .background(.gray.opacity(0.15))
                            .foregroundStyle(.primary)
                            .clipShape(RoundedRectangle(cornerRadius: 6))
                        }
                        .gridCellColumns(2)
                    }
                    GridRow {
                        Menu {
                            Picker(selection: $layer.shadow) {
                                ForEach(shadowValues, id: \.self) { value in
                                    Text(String(format: "%.0f px", value))
                                        .tag(value)
                                }
                            } label: {}
                        } label: {
                            HStack(spacing: 2) {
                                Image(systemName: "arrow.up.and.down")
                                Image(systemName: "shadow")
                            }
                            .frame(maxWidth: .infinity)
                            .frame(height: 35)
                            .background(.gray.opacity(0.15))
                            .foregroundStyle(.primary)
                            .clipShape(RoundedRectangle(cornerRadius: 6))
                        }
                    }
                }
                .colorPickerSheet(isPresented: $showingColorPicker, selection: $layer.font.color)
            }
            Rectangle()
                .fill(.clear)
                .frame(height: 1)
        }
        .overlay {
            if editorModel.layerSelection == nil {
                Text("No layer selected")
                    .font(.subheadline)
                    .foregroundStyle(.secondary)
            }
        }
    }
}

#Preview {
    LayerFontEditor(editorModel: EditorModel())
}
