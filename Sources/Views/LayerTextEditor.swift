//
//  LayerTextEditor.swift
//  Overlay
//
//  Created by Adam Oravec on 10/03/2024.
//

import SwiftUI

struct LayerTextEditor: View {
    
    @Bindable var editorModel: EditorModel
    @FocusState private var isTextFieldFocused: Bool
    
    var body: some View {
        ScrollView {
            if let layer = editorModel.layerSelection {
                @Bindable var layer = layer
                TextField("Text", text: $layer.text, axis: .vertical)
                    .lineLimit(5...10)
                    .textFieldStyle(.roundedBorder)
                    .focused($isTextFieldFocused)
            }
            Rectangle()
                .fill(.clear)
                .frame(height: 1)
        }
        .scrollDismissesKeyboard(.interactively)
        .overlay {
            if editorModel.layerSelection == nil {
                Text("No layer selected")
                    .font(.subheadline)
                    .foregroundStyle(.secondary)
            }
        }
        .onAppear {
            isTextFieldFocused = true
        }
    }
}

#Preview {
    LayerTextEditor(editorModel: EditorModel())
}
