//
//  ExportView.swift
//  Overlay
//
//  Created by Adam Oravec on 15/02/2024.
//

import SwiftUI

struct ExportView: View {
    
    let inputImage: UIImage
    let editorModel: EditorModel
    
    @Environment(\.dismiss) private var dismiss
    @State private var result: Result<WrappedImage, ExportError>?
    @State private var exportFormat: ExportFormat = .png
    
    var body: some View {
        NavigationStack {
            VStack {
                if let result {
                    Picker("Export Format", selection: $exportFormat) {
                        ForEach(ExportFormat.allCases, id: \.rawValue) { format in
                            Text(format.rawValue).tag(format)
                        }
                    }
                    .pickerStyle(.segmented)
                    .padding(.horizontal)
                    Spacer()
                    if case .success(let renderedImage) = result {
                        let image = Image(uiImage: UIImage(data: renderedImage.data)!)
                        image
                            .resizable()
                            .scaledToFit()
                            .draggable(renderedImage)
                        Spacer()
                        ShareLink(item: renderedImage, preview: SharePreview(renderedImage.id.uuidString, image: image))
                            .padding(.bottom)
                    } else if case .failure(_) = result {
                        ContentUnavailableView("Export Failed", systemImage: "exclamationmark.triangle", description: Text("Unable to export in this format."))
                        Spacer()
                    }
                } else {
                    ProgressView()
                }
            }
            .navigationTitle("Export")
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .confirmationAction) {
                    Button("Done") {
                        dismiss()
                    }
                }
            }
        }
        .task {
            await renderLayers(format: .png)
        }
        .onChange(of: exportFormat) { _, format in
            Task {
                await renderLayers(format: format)
            }
        }
    }
    
    @MainActor private func renderLayers(format: ExportFormat) async {
        let renderView = RenderView(image: inputImage, editorModel: editorModel)
        let imageRenderer = ImageRenderer(content: renderView)
        if let image = imageRenderer.uiImage {
            let compressedImage = compressImage(image, format: format)
            switch compressedImage {
            case .success(let data):
                self.result = .success(WrappedImage(data))
            case .failure(let error):
                self.result = .failure(error)
            }
        }
    }
    
    @MainActor private func compressImage(_ image: UIImage, format: ExportFormat) -> Result<Data, ExportError> {
        switch format {
        case .png:
            if let compressedData = image.pngData() {
                return .success(compressedData)
            }
        case .jpeg:
            if let compressedData = image.jpegData(compressionQuality: 0.2) {
                return .success(compressedData)
            }
        case .heic:
            if let compressedData = image.heicData() {
                return .success(compressedData)
            }
        }
        return .failure(ExportError.unableToCompress)
    }
}

extension ExportView {
    enum ExportFormat: String, CaseIterable {
        case png
        case jpeg
        case heic
    }
    
    enum ExportError: Error {
        case unableToCompress
    }
}
