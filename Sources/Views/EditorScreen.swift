//
//  EditorScreen.swift
//  Overlay
//
//  Created by Adam Oravec on 13/02/2024.
//

import SwiftUI

struct EditorScreen: View {
    
    let image: UIImage
    
    @Environment(\.dismiss) private var dismiss
    @Environment(\.colorScheme) private var colorScheme
    
    @State private var editorModel = EditorModel()
    
    var body: some View {
        VStack(spacing: 0) {
            Spacer()
            EditorCanvas(image: image, editorModel: editorModel)
            Spacer()
            EditorCanvasInspector(editorModel: editorModel)
                .containerRelativeFrame(.vertical) { value, axis in
                    value * 0.4
                }
                .frame(maxWidth: 500)
                .background {
                    if colorScheme == .light {
                        Rectangle().fill(.background)
                    } else {
                        Rectangle().fill(.background.secondary)
                    }
                }
                .clipShape(RoundedRectangle(cornerRadius: 8))
                .shadow(radius: 4)
                .safeAreaDependentPadding()
        }
        .toolbar {
            topToolbar
        }
        .sheet(isPresented: $editorModel.showingResultSheet) {
            ExportView(inputImage: image, editorModel: editorModel)
        }
        .onAppear {
            editorModel.layers = [Layer()]
            editorModel.layerSelection = editorModel.layers[0]
        }
    }
    
    var topToolbar: some ToolbarContent {
        ToolbarItem(placement: .topBarTrailing) {
            Button {
                editorModel.showingResultSheet = true
            } label: {
                Label("Export", systemImage: "square.and.arrow.up")
            }
        }
    }
}

#Preview {
    EditorScreen(image: UIImage())
}
