//
//  ColorPickerButton.swift
//  Overlay
//
//  Created by Adam Oravec on 21/03/2024.
//

import SwiftUI

struct ColorPickerButton: View {
    
    @Binding var selection: Color
    
    @State private var showingColorPicker = false
    
    var body: some View {
        Button {
            showingColorPicker = true
        } label: {
            selection
                .clipShape(RoundedRectangle(cornerRadius: 6))
                .frame(maxWidth: .infinity)
                .frame(height: 35)
                .overlay {
                    RoundedRectangle(cornerRadius: 6)
                        .strokeBorder(Color.secondary.opacity(0.2), lineWidth: 1)
                }
        }
        .buttonStyle(.borderless)
        .colorPickerSheet(isPresented: $showingColorPicker, selection: $selection)
    }
}
