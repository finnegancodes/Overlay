//
//  LabeledSlider.swift
//  Overlay
//
//  Created by Adam Oravec on 13/02/2024.
//

import SwiftUI

struct LabeledSlider: View {
    
    @Binding var value: CGFloat
    let range: ClosedRange<CGFloat>
    let label: LocalizedStringKey
    let defaultValue: CGFloat
    
    init(_ label: LocalizedStringKey, value: Binding<CGFloat>, in range: ClosedRange<CGFloat>, defaultValue: CGFloat) {
        _value = value
        self.range = range
        self.label = label
        self.defaultValue = defaultValue
    }
    
    var body: some View {
        HStack {
            Text(label)
            Slider(value: $value, in: range, step: 0.01)
            Button {
                value = defaultValue
            } label: {
                Label("Reset", systemImage: "arrow.counterclockwise.circle")
                    .labelStyle(.iconOnly)
            }
        }
    }
}

#Preview {
    LabeledSlider("X", value: .constant(0), in: 0...1, defaultValue: 0)
}
