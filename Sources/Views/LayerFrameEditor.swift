//
//  LayerFrameEditor.swift
//  Overlay
//
//  Created by Adam Oravec on 10/03/2024.
//

import SwiftUI

struct LayerFrameEditor: View {
    
    @Bindable var editorModel: EditorModel
    
    @State private var showingColorPicker = false
    
    var body: some View {
        ScrollView {
            if let layer = editorModel.layerSelection {
                @Bindable var layer = layer
                LabeledSlider("Width", value: $layer.size.width, in: 0...1, defaultValue: 0.5)
                LabeledSlider("Height", value: $layer.size.height, in: 0...1, defaultValue: 0.2)
                HStack {
                    Text("Background")
                    Spacer()
                    ColorPickerButton(selection: $layer.background)
                        .frame(width: 80)
                }
                .colorPickerSheet(isPresented: $showingColorPicker, selection: $layer.background)
                HStack {
                    Text("Position")
                    Spacer()
                    Button("Reset") {
                        layer.position = CGPoint(x: 0.5, y: 0.5)
                    }
                    .buttonStyle(.bordered)
                }
            }
            Rectangle()
                .fill(.clear)
                .frame(height: 1)
        }
        .overlay {
            if editorModel.layerSelection == nil {
                Text("No layer selected")
                    .font(.subheadline)
                    .foregroundStyle(.secondary)
            }
        }
    }
}

#Preview {
    LayerFrameEditor(editorModel: EditorModel())
}
