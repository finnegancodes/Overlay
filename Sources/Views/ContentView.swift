//
//  ContentView.swift
//  Overlay
//
//  Created by Adam Oravec on 13/02/2024.
//

import SwiftUI

struct ContentView: View {
    
    @State private var path: [UIImage] = []
    
    var body: some View {
        NavigationStack(path: $path) {
            ImageChooserScreen { image in
                path.append(image)
            }
            .padding(10)
            .navigationDestination(for: UIImage.self) { image in
                EditorScreen(image: image)
            }
        }
    }
}

#Preview {
    ContentView()
}
