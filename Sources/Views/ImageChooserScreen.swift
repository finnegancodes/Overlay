//
//  ImageChooserScreen.swift
//  Overlay
//
//  Created by Adam Oravec on 13/02/2024.
//

import SwiftUI
import PhotosUI

struct ImageChooserScreen: View {
    
    let onChoose: (UIImage) -> Void
    
    @State private var showingImageChooser = false
    @State private var showingSizeChooser = false
    @State private var selectedItem: PhotosPickerItem?
    
    var body: some View {
        VStack(spacing: 8) {
            Spacer()
            Text("Overlay")
                .font(.largeTitle.bold())
            Text("Put text over images.")
                .foregroundStyle(.secondary)
            Spacer()
            Spacer()
            VStack {
                PhotosPicker("Choose Image", selection: $selectedItem, matching: .images)
                    .buttonStyle(.borderedProminent)
                Button("New Blank Image") {
                    showingSizeChooser = true
                }
                .buttonStyle(.bordered)
            }
            Spacer()
            Text("v\(Bundle.main.releaseVersionNumber) (\(Bundle.main.buildVersionNumber))")
                .foregroundStyle(.secondary)
                .font(.subheadline)
        }
        .onDrop(of: [.image], isTargeted: nil) { providers in
            handleDrop(providers: providers)
        }
        .onChange(of: selectedItem) { _, selectedItem in
            guard let selectedItem else { return }
            Task {
                await loadImage(fromItem: selectedItem)
            }
        }
        .sheet(isPresented: $showingSizeChooser) {
            NewImageForm(onCreate: onChoose)
        }
    }
    
    @MainActor
    private func handleDrop(providers: [NSItemProvider]) -> Bool {
        guard let provider = providers.first else { return false }
        Task {
            await loadImage(fromProvider: provider)
        }
        return true
    }
    
    @MainActor
    private func loadImage(fromItem item: PhotosPickerItem) async {
        do {
            if let data = try await item.loadTransferable(type: Data.self) {
                if let image = UIImage(data: data) {
                    onChoose(image)
                    self.selectedItem = nil
                } else {
                    print("No image")
                }
            } else {
                print("No data")
            }
        } catch {
            print("Failed to load data: \(error.localizedDescription)")
        }
    }
    
    @MainActor
    private func loadImage(fromProvider provider: NSItemProvider) async {
        let _ = provider.loadDataRepresentation(for: .image) { data, error in
            if let data = data {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self.onChoose(image)
                        self.selectedItem = nil
                    }
                } else {
                    print("No image")
                }
            } else if let error = error {
                print("Failed to load data: \(error.localizedDescription)")
            }
        }
    }
}

#Preview {
    ImageChooserScreen { _ in }
}
