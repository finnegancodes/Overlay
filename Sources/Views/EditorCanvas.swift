//
//  EditorCanvas.swift
//  Overlay
//
//  Created by Adam Oravec on 13/02/2024.
//

import SwiftUI

struct EditorCanvas: View {
    
    let image: UIImage
    let editorModel: EditorModel
    
    @State private var snapGuides = SnapGuides()
    
    var body: some View {
        Image(uiImage: image)
            .resizable()
            .scaledToFit()
            .onTapGesture {
                print("tap")
                editorModel.layerSelection = nil
            }
            .overlay {
                GeometryReader { proxy in
                    ForEach(editorModel.layers) { layer in
                        EditorCanvasLayerView(layer, size: proxy.size, isSelected: editorModel.layerSelection == layer, snapGuides: $snapGuides) {
                            editorModel.layerSelection = layer
                        } onEdit: {
                            editorModel.inspectorSelection = .textEditor
                        }
                    }
                    
                    if snapGuides.x {
                        Rectangle()
                            .fill(Color.accentColor)
                            .frame(width: 1, height: proxy.size.height)
                            .position(x: 0.5 * proxy.size.width, y: 0.5 * proxy.size.height)
                    }
                    
                    if snapGuides.y {
                        Rectangle()
                            .fill(Color.accentColor)
                            .frame(width: proxy.size.width, height: 1)
                            .position(x: 0.5 * proxy.size.width, y: 0.5 * proxy.size.height)
                    }
                }
            }
            .readSize { size in
                editorModel.scale = size.width / image.size.width
            }
            .coordinateSpace(name: "canvas")
            .clipped()
            .sensoryFeedback(.selection, trigger: snapGuides) { _, snapGuides in
                snapGuides.x || snapGuides.y
            }
    }
}

#Preview {
    EditorCanvas(image: UIImage(), editorModel: EditorModel())
}
