//
//  EditorCanvasLayerView.swift
//  Overlay
//
//  Created by Adam Oravec on 14/02/2024.
//

import SwiftUI

struct EditorCanvasLayerView: View {
    
    @Bindable var layer: Layer
    let size: CGSize
    let scale: Double
    let isSelected: Bool
    let onSelect: () -> Void
    let onEdit: () -> Void
    @Binding var snapGuides: SnapGuides
    
    @State private var offset: CGPoint = .zero
    @State private var isDragging: Bool = false
    
    init(_ layer: Layer, size: CGSize, scale: Double = 1, isSelected: Bool, snapGuides: Binding<SnapGuides>, onSelect: @escaping () -> Void, onEdit: @escaping () -> Void) {
        self.layer = layer
        self.size = size
        self.scale = scale
        self.isSelected = isSelected
        _snapGuides = snapGuides
        self.onSelect = onSelect
        self.onEdit = onEdit
    }
    
    var dragGesture: some Gesture {
        DragGesture(minimumDistance: 1, coordinateSpace: .named("canvas"))
            .onChanged { value in
                if !isSelected {
                    onSelect()
                }
                
                if !isDragging {
                    isDragging = true
                    offset = getOffset(for: value.location)
                }
                
                var x = (value.location.x - offset.x) / size.width
                var y = (value.location.y - offset.y) / size.height
                
                let snapToX = (0.48...0.52).contains(x)
                let snapToY = (0.48...0.52).contains(y)
                
                if snapToX {
                    x = 0.5
                }
                
                if snapToY {
                    y = 0.5
                }
                
                snapGuides.x = snapToX
                snapGuides.y = snapToY
                
                layer.position = CGPoint(x: x, y: y)
            }
            .onEnded { _ in
                snapGuides.off()
                isDragging = false
                offset = .zero
            }
    }
    
    var tapGesture: some Gesture {
        TapGesture()
            .onEnded {
                onSelect()
            }
    }
    
    var doubleTapGesture: some Gesture {
        TapGesture(count: 2)
            .onEnded {
                onEdit()
            }
    }
    
    var body: some View {
        ZStack {
            layer.background
            Text(layer.text)
                .font(
                    layer.font.name == .stock ?
                        .system(size: layer.font.size * scale) :
                        .custom(layer.font.name.rawValue, size: layer.font.size * scale)
                )
                .italic(layer.font.isItalic)
                .bold(layer.font.isBold)
                .fontDesign(layer.font.design.raw)
                .multilineTextAlignment(layer.font.alignment.raw)
                .foregroundStyle(layer.font.color)
                .shadow(color: layer.shadow == 0 ? Color.clear : Color.black, radius: layer.shadow * scale)
                .shadow(color: layer.shadow == 0 ? Color.clear : Color.black, radius: layer.shadow * scale)
        }
        .frame(width: layer.size.width * size.width, height: layer.size.height * size.height)
        .clipped()
        .overlay {
            if isSelected {
                Rectangle()
                    .strokeBorder(Color.accentColor, lineWidth: 2)
            }
        }
        .position(x: layer.position.x * size.width, y: layer.position.y * size.height)
        .gesture(tapGesture)
        .simultaneousGesture(doubleTapGesture)
        .simultaneousGesture(dragGesture)
        
    }
    
    private func getOffset(for tapPoint: CGPoint) -> CGPoint {
        let center = CGPoint(x: layer.position.x * size.width, y: layer.position.y * size.height)
        let offset = CGPoint(x: tapPoint.x - center.x, y: tapPoint.y - center.y)
        return offset
    }
}

#Preview {
    EditorCanvasLayerView(Layer(), size: CGSize(width: 500, height: 400), scale: 1, isSelected: false, snapGuides: .constant(SnapGuides())) {
        
    } onEdit: {
        
    }
}
